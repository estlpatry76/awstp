import config from "config";
import { DataSource } from "typeorm";

import User from "../models/user.model";
import Thread from "../models/thread.model";
import Post from "../models/post.model";

const database = new DataSource({
  type: "sqlite",
  database: config.get<string>("db.data") + ".sqlite",

  synchronize: true,
  entities: [User, Post, Thread],
});

export default database;
